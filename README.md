# Ngerukewid_landSnailSurvey_2019

Species diversity data and some preliminary analyses for the Ngerukewid land snail survey Rundell & Czekanski-Moir did in 2019. Hopefully we'll submit this for publication in 2023/2024. 

Data are stored as tab-delimited .txt files; Most analyses are run in R in the vegan and iNEXT libraries. 
For more info on computing diversity stats with these packages, please see: 
https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12613
https://cran.r-project.org/web/packages/vegan/vignettes/diversity-vegan.pdf 
